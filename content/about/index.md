---
title: "about selestium modular"
description: "information about the practice and the store"
slug: ""
featured: ""
keywords: ""
categories: 
    - "Index"
    - "about"
date: 2023-08-15T00:24:39+02:00
layout: minimal
draft: false
---

*selestium modular* is the modular synthesis production arm of the [tranxxeno lab](https://tranxxenolab.net), the nomadic artistic research laboratory founded by the artist, writer, and xenologist [Adriana Knouf](https://tranxxenolab.net/people/adriana_knouf/). *selestium modular* was started to investigate the sonic artifacts found in dig AG-105 and re-create them using contemporary techniques. 

All modules produced by *selestium modular* can thus be seen as extensions of the artistic research and practice of the [tranxxeno lab](https://tranxxenolab.net). 

All modules can be purchased from the [selestium vending](https://vending.selestium.net) website. 

The modules are handmade in Bijlmer, Amsterdam, The Netherlands.

# Mailing list

You can sign-up for a low-volume mailing list for news, new and updated modules, and other information about selestium modular by [following this link](https://246ac891.sibforms.com/serve/MUIFAD6gtNQpgFFmu3k9IxYraQa9NSKX4njFkpzK8cIbjANVsUYeeqVtpGW28lvTjrjLRx3IeFKdlRImnMMeUwjgVuIxgPfyv8OE82RPw5oAeuoH7TnuzKqXV7OWPINFubmwM2fQVvCRfhX-dp07YYQa_7HOfWISVrCkLem82_bsKDgg75ZpFYwTI4xu6Bwvv4oT0WpB-bg6-qHr).

# Discord

You can join the [selestium discord](https://discord.gg/jxkPk57gFM) that has specific channels for *selestium modular* discussions and updates. Please note that as Adriana runs *selestium modular* as a side job alongside other work, she may not always be available.

# E-mail

If you would like to contact us directly please e-mail <a href="mailto:modular@selestium.net">modular@selestium.net</a>.


<!--In fact, all modules are sold through the [tranxxeno lab vending](https://vending.tranxxenolab.net) website.-->

