---
title: "Legend"
description: "or, how to understand the iconography"
slug: ""
featured: ""
keywords: ""
categories: 
    - "Index"
    - "page"
date: 2023-08-16T16:40:58+02:00
draft: false
thumbnail: 
    url: "img/selestium002_square_purple.png"
layout: minimal
---

{{< mimage src="images/number_system_formatted_whitebg.png" caption="Numbering system of selestium modular modules" >}}

{{< mimage src="images/input_output_whitebg.png" caption="Input and output dynamic of selestium modular modules" >}}
