---
title: "About dig AG-105"
description: "or, how we know how to make these modules"
slug: ""
featured: ""
keywords: ""
categories: 
    - "Index"
    - "page"
date: 2023-08-15T13:55:52+02:00
draft: false
thumbnail: 
    url: "img/selestium002_square_purple.png"
layout: minimal
---



{{< mimage src="/images/dig_AG-105.png" caption="Timestream-reconstructed image of dig AG-105, Venus evening cycle 128" >}}


Dear A–,

I'm able to catch a few moments to update you on things here. Sorry for not writing for a while...I know that I say that a lot, but in this case I am truly sorry, because I think both you and the ESRG will be amazed by the finds here.

We've termed the dig AG-105...you know what the letters and numbers mean, and why I'm not divulging the precise location in this quanta--looters sadly still exist. Anyway, AG-105 is in a place that used to be extremely flat...flat as a pancake you might say. Yet the dig location itself is a bit elevated above the surrounding area. Or at least that's what we think from the sedimentation patterns...of course this area was under water during the Transitioning Times, so we're going a bit on conjecture here. Further topological analysis (both in terms of geography as well as temporality) will tell us more. We have a few letters, but no definitive place name yet: BJLMR.

Besides the flooding that ravaged the area, it seems like it's been relatively undisturbed since...well, since the artifacts we found were left here. And wow what artifacts they are! You, as well as the ESRG, are going to have so much fun discovering what these things do and trying to recreate them using our technology.

Oh wait, I just heard the bell...I need to end this quanta now. You know how things are on digs...miss the bell, miss dinner. More soon!

In solidarity,

K–

—

Dear A--,

Wow, I didn't expect you to respond in the way that you did! I know you and the ESRG haven't gotten along for a while, but don't make me choose between the two. As one of the first members of the Electric Sounds Research Group I still believe wholeheartedly in their mission: to make available to sound makers of today the techniques and capabilities of those who lived just before the Transitioning Times. I know you ardently believe in a mimetic fidelity--down to the nano level even!--to the ancient functionality of the silico-based devices. But you know as well as I do that only those of us who have a fully-functioning quanto-electro-genetic laboratory--*cough* like you *cough*--can recreate these devices at home. Most of us don't have your facility with material manipulation. And most of us haven't studied the literature of the past millenia as closely as you have--because most of us leave the house occasionally to see other people.

Sorry. That was a bit harsh. But I'm not going to erase it and rewrite it because I think it's important to say.

A--, I wish you were here on the dig with me, it's been decades since you've gotten your hands dirty in the field. Will you consider coming? I can send you the precise location in an entangled quanta. I know your own transition hasn't gone in the way that you hoped it would, but that doesn't mean you have to stay at home like a hermit.

Here, let me draw a card for you...holy fuck: The Fool. No, I'm not making this up. No, I didn't just choose The Fool to try and get you here...that's exactly what came up when I pulled from the deck! See? Even the Tarot wants you to take a leap. The dog will accompany you on your journey 😏🐕‍🦺

Let me leave you with something. We've done a bit more study of the topological aspects of the site. Accounting for the shifting sands due to wave action, as well as the precession of the earth...we believe AG-105 had an astronomical orientation! Indeed, we think they might have been able to tap into our own timestreams through hyperstition. We need to do a lot more research until we're able to say such a thing publicly, but it's not out of the realm of possibility.

Of course I'm writing all of my notes for the Selestium...I wouldn't think of abandoning my contributions to your life's work just because I'm on an ESRG-funded dig.

With hope,

K– 

—

Dear A--,

You make it very difficult to care about you sometimes. Your obstinacy is without comparison. Fine, stay at home. I'm still smitten and thus I'll continue to send you infos.

I do have to thank you for the reference though. I hadn't heard of *Overlay: Contemporary Art and the Art of Prehistory* by Lucy Lippard. It's quite remarkable what they were able to write without hyperstition technology, without the ability to see the merging of timestreams. Lippard was definitely onto something when she wrote that the practices of the pre-ancients and the then-contemporaries overlapped...she just didn't have the right terminology. It's like another historical fact that I've learned. Did you know that at the same time Lippard was writing not everyone believed in the complete transformational ability of our body? And I use the word "believe" specifically, as the proof that they were wrong was all around them...people walking around who had begun a form of transformation. Yet they stuck to the belief that the body was fixed and immutable, even while their  own bodies transformed from a child to an adult to an old person. I guess it took the Transitioning Times for people to realize the necessity of regular and continual transformation, even if it means perhaps greater risk, as our bodies are, thankfully, not completely predictable machines.

Sorry...I know it's a bit of a sore subject for you. But I assure you that you'd be welcome here...we're all a palimpsest of occasional transformational mishaps, as none of us at the dig have followed the usual path 😏

Anyway, Venus is rising at the horizon, which means I've stayed up too late, and...wait. Wait. WAIT! I think I've figured something out. Oh A– this might be incredible if true!

Love,

K–

—

Dear A--,


{{< mimage src="images/logo_purple_upscaled.png" ratio="1x1" outer="image-float-right" inner="image-radius-3" caption="Sigil found throughout AG-105 artifacts" >}}

Okay okay okay I'll explain. We've been finding this sigil throughout the dig on various artifacts...and yes, I'll share info about the materials soon enough! I've been completely flummoxed about what it means. I thought the three stars were three *separate* stars, and I had been pouring over the timestream-adjusted star charts to see if I could find an asterism that matched. Problem is...there were literally *thousands* of asterisms that it could be, even at a limit magnitude of -7.0. (Yes, I'm using the old units, I'm deep in the headspace of the dig, deal with it.) But, while writing my letter to you and seeing Venus rise I realized that in fact it wasn't three separate stars, *it was the same star*. And it wasn't a star at all, but a planet! The makers of these materials had twisted the timestreams between their time, our time, and the pre-ancient times and realized the importance of the triple aspect of Venus! Venus as morning star, Venus as evening star, and Venus as the absent star (or the star of the underworld, or the star of transition).

Oh, my closing last message? I was just excited, my emotions got the best of me. I know the distance that we should keep. I'll try to not let it happen again.

More soon,

K–

—

Dear A--,

I know I shouldn't send two quanta in one night, especially after getting drunk on the stars, but I feel compelled to send you my retelling of our founding tale about Venus and what we learned during the Transitioning Times:

> There once was a person who lived on a vast, undulating field. Let's call them Semsta. The undulations of the land were the only things that seemed to have any variation for Semsta. Semsta and their cohort did the same work each and every day. It doesn't matter what work it was: farming, ceramics, writing, observing the stars, repairing the calcs, teaching. It was the same, day in and day out. It had been this way since before Semsta was born, and it was going to be this way long after Semsta had died.

> One day the astrologers had placed macros along the land to let everyone know that the Venus Apex Festival was approaching, when Venus shifted from an evening star to a morning star, or vice versa. Venus Apex was one of the most joyous times of the year, for it foretold the movement into the next cycle, and the continuation of things. One night Venus set in the west, and in the morning Venus rose in the east. It was a sign that things would continue, as they always had, and as they always would. 

> It should make Semsta happy, just like everyone else in their cohort and all the other cohorts, but this year it didn't. Semsta felt odd, like something was wrong with this continuation, this ceasingness, this relentlessness of the cycles. When might there be something that changed? When might there be something that is different? Semsta knew that the continuation of the cycles meant continued existence for them and for everyone they loved, but now they yearned for something different. Couldn't there be something more than the repetition of the same? Couldn't there be a repetition with a difference?

> Semsta wasn't able to ponder these questions too much, because the appearance of the macros meant the mass gathering at the Henge. Parties, dancing, electric music, food, wine, mixing of cohorts, melding of bodies--it was a proper festival. Semsta's cohort collected what they would need and what they would share, and arrived a couple days before the purported Apex night.

> On that fateful Apex night, everyone gathered and looked to the west. The Sun set, and shortly thereafter, Venus as well. Everyone at the Henge broke out in song and dance, drank heavily and ate even more, and by the time the night was thin, everyone was some form of tipsy. Now oriented to the east and at the appointed time according to the astrologers, everyone, including Semsta (who was themself also quite tipsy, despite their mood), looked for the first glimpse of Venus rising. There was a legend that the first who saw Venus rise at the Apex Festivals would be able to communicate with their non-human of choice, so everyone was fixated on the same point on the horizon. The appointed time came...and nothing. A few minutes passed. Still nothing. Mummurs began to spread through the crowds. Were the astrologers wrong in their calculations? Such a thing hadn't happened for centuries, ever since the development of the calcs. Semsta looked to the front of the crowd, where the astrologers had some of the best views. Semsta could see that they were conversing amongst themselves, re-doing the figures on their calcs, flummoxed as to what was going on. A little kid shouted, "Venus!", and the crowd cheered. But it was just a sliver of the Sun, the eternal foreteller of day and night, and parents and cohort wardens could be heard guiding the little ones to not burn their eyes by looking at the phantom Venus.

> After a while everyone dispersed to their camps, confused, dejected, worried. Had the cycle finally broken down?

A--, I can't continue tonight, I'm getting too emotional, I need to close my eyes to the cosmos sometimes in order to continue living here. I'll come back to this later.

Lost in the vicinity of Arcturus,

K–

—

Dear A–,

I found my way back in the blueish hours of the morning, around the same time that Semsta and the others were waiting for the rising of Venus on that Apex Festival morning. I muddled through the day, oblivious to the artifacts that normally hold my attention. I couldn't wait until I was back in my tent and had time to continue my retelling of the tale to you, in this quanta, right now:

> No-one slept well after that fateful night. No-one sleeps well anyway after the Venus Apex festival due to the gorging that happens, but this was different. An unsettled mood had diffused through the Henge and its environs, like a fog that wouldn't lift no matter the intensity of the Sun. Semsta wandered among the camps, debated the events with their current and former cohorts. No-one knew what had occurred, but as the sun passed its highest point in the sky and the morning turned to afternoon, a curious thought began to filter throughout the camps: that perhaps there was something new in the universe, that the calcs were off because of some new understanding that the astrologers would have to take into account. People began to hope that the evening would reset the cycle, that the astrologers would have new data to pour over for the coming months, and everyone could scatter back to their home locales.

> The sun set, night came, and most entered into a fitful sleep, desiring to wake up an hour or so before dawn to hopefully see the rising of Venus. Semsta couldn't sleep at all--whenever they closed their eyes all they saw were flashes of light that were constantly undulating and transforming--and thus they went and kept watch next to the astrologers. 

> "We're sure tonight will be the night," one said.

> "This new data is going to be hard to integrate into the equations," another said.

> "How will we ever do it!" said the youngest, obviously concerned about the mountains of numbers they would have to enter into the calcs over the coming months.

> Just before the appointed time, a small crowd began to converge around the astrologers, a fraction of what had been there during the previous festival night. The time came...and nothing. A few minutes later...nothing. By this time the mummers of those present had woken up those who were not. As time passed and Venus had still not risen, the glow of the morning sun began to spread into the sky from the east. Fearful of what the absence of Venus might mean, a strident din began to rise from the crowd.

> The facilitating astrologer came to the front of the crowd and motioned with their hands for everyone to calm down. After a few moments of doing this, everything and everyone was silent. The astrologer spoke.

> "We don't know what is happening," they said. "We believed in the veracity of the numbers produced by our calcs. Obviously our understanding of the cosmos is incomplete in some way. We humble ourselves before this and vow to stay here until Venus has risen again, however long that may be." The youngest astrologer could be heard whimpering, quietly. 

> "Please head home," the facilitating astrologer continued, "there's nothing more to see here, we will spread macros when we know Venus will rise again. For Venus will do so, and life will continue as it did before!"

> This seemed to placate most of the crowd. They weren't exactly cheering, but they also weren't as dejected as before. The crowd slowly dispersed and began to pack up their camps. Semsta joined their own camp and did the same. But as their cohort were about to leave Semsta felt an undeniable urge to stay put--to not leave, to join the astrologers, to wait until Venus returned. Semsta's job was certainly not as an astrologer--far from it. Yet stay here they must. It felt like the most important choice in their life, one that had already been full of what seemed to be important choices.

> "I'm staying," Semsta informed their cohort warden. "I need to be here while we wait for Venus to rise."

> Jesp, the warden, replied: "Really? You know nothing of astrology, nor of prediction. We need your hands in the cohort's practices. We need your body in the pile at night for warmth. Who will you do that with here?"

> Semsta answered, "It's like when I took leave for my passage, it's like when all of us take leave for our passages, we manage on our own for a while."

> "That's different. No-one ever does a second passage."

> Semsta said, defiantly, in a way that surprised them: "Maybe we need to!"

> With that Jesp huffed off, leaving Semsta without the satisfaction of seeing their expression. Jesp was always a bit hot-headed but what Semsta wanted to do was unprecedented. They expected at least a bit more of a response from Jesp than simply leaving. Semsta was now worried about what they had done. Maybe there's a reason why no-one ever does a second passage in their life? Maybe Semsta had severed their connection with their cohort, and would need to do the arduous (yet also invigorating) process of finding a new one? Semsta couldn't think of those things now. They needed to talk to the astrologers, convince them that they could stay, and setup camp for the night. It would be the beginning of a long wait for Venus to rise again.

Capella has traveled from the eastern horizon to around 30 degrees high...and its high time for me to sleep now. Sending this quanta with an incomplete story, again. I wish I could be telling it to you in person, rather than in this distanced way. *Sigh*.

Under the stars,

K–

—

Dear A–,

I feel sometimes as you don't quite appreciate the beauty of our stories, even as you preach them through your creations and your own writings. But maybe again it's because quanta--though practically instantaneous--convey oh so little.

It's no matter. Like I've said many times before, somehow I'm smitten, somehow I'm just as obstinate as you, and somehow I yearn for those rare moments when you look up from the work to look at me. But of course that's not possible to see here. Dammit A--! Just read the entangled quanta I sent, please? I obviously know you haven't opened it yet, as the superposition is still standing.

Maybe if I finish the story. Let me continue:

> The astrologers found Semsta's desire to stay perplexing, but as long as Semsta stayed out of their way, they were content to let Semsta stay for a while--but no instruction from them was forthcoming. An apprentice-based passing this would not be.

> Night after night passed and Venus still had not risen. The astrologers were running out of variations to put into their code, perturbations to add to their systems, their calcs were running dry and would need biofilling soon. Frustrated with the lack of progress one morning, Semsta made an impetuous decision: they would walk to the horizon where Venus was supposed to rise, and see if there was something that was blocking Venus from appearing. This line of thought made absolutely no sense: Semsta, like everyone one else of course, knew that this planet was round, that at the horizon was just more land because, well, they lived on a sphere. There was no reason for Semsta to want to do such a silly thing. But there was something that had lodged into Semsta's brain from their readings of the pre-ancients: that there were stories about a flat earth, that at the edge was a portal to go to another place, an "underworld" (which Semsta always thought was a reference to another dimension). Perhaps there was some credence to those stories, Semsta wasn't sure. But one thing they did know: they couldn't sit amongst the astrologers anymore.

> Semsta gathered their provisions and was grateful for the small amount of things they had brought to the Apex Festival. They set out, waving to the oblivious astrologers who had gotten ever more frustrated as the days passed and their predictions failed.

> Day turned to night, night to day, day to night, night to day...the undulating land continued, as expected of someone living on a sphere. Semsta had already traveled much further than they had ever gone, and still, there was nothing to indicate that anything was amiss. As the night turned to day again, Semsta resolved that if they didn't find anything at the end of this day of walking, they would head back. Their own food generator would need biofilling soon, otherwise they would have to resort to foraging.

> Today's heat was much more intense than yesterdays, and by midday all of their skin was covered in sweat. They kept on, determined to go as far as they could on their last day.

> The eastern sky had been dimming for a long time, the setting Sun still warming the back of Semsta's head. Just as Semsta was about to set down their load for the night, they took one more step...and fell into a void.

Sleep well, A–,

K–

—

Dear A–,

Is that a bit of a crack I sense in your writing? A sliver of your emotional side beginning to stream out? Don't let it grow too wide or I won't know where A– has gone!

I jest, I jest, I'm just heartened to know that my writing is affecting you a bit.

You still haven't opened the entangled quanta. Let me tempt you with this: we've found a module whose name we've deciphered as "XENOKINETICS". Now, if that doesn't pique your interest, I don't know what will!

Okay, you demanded--in the nicest way that only you can do--that I continue the story. And so I will do so, by your command:

> Tumbling, tumbling, tumbling, Semsta felt no air flowing by their body, no pull towards one way or another, no directionality. They were in a void, or in weightlessness--the two being not so distant. Slowly they became oriented vertically, like a compass needle left to point towards north and south. A ground met their feet, gently. Semsta found themselves on a surface--not exactly a floor, not exactly a ground, but rather some combination of materials that were completely unknown to them. 

> In the distance, directly in front of them, Semsta noticed a flickering. Not of a candle, not of a fire, but of something much more impressive, something that seemed to take up the entire horizon. Semsta was drawn to it, like most beings would be. The flickering became more intense the longer Semsta walked towards it. After what seemed to be an interminable time of walking, the flickering, which was really more of an irregular, slow pulsing, seemed to surround Semsta from all sides, as if they were in a dome...or inside the light itself.

> A mummur began to rise, not of recognisable sounds but rather of a being *trying* to say something understandable. Semsta strained to understand, the phonemes sounding *somewhat* recognisable, but only at the edge of comprehension. The phonemes began to blend in complicated ways, until at a certain point Semsta was able to make out what was being said.

> "Why have you come here?"

> Semsta was startled; not exactly what they were expecting. "Venus has disappeared. I'm trying to find them."

> "Well, you have done so. And you have disturbed me!" The pulsing increased in intensity, both in its frequency but also in how bright it was. Semsta felt completely pummeled by it, as if they were about to be crushed by the weight of the light. "Stop, please, I beg you!", Semsta cried out.

> "Why should I?", the voice replied.

> "I've merely come to understand why Venus hasn't returned as predicted. Their absence is causing problems for us."

> The intensity of the light quickly ramped down to its previous level. "I needed time for transformation." It was only at this moment that Semsta realized the situation: they were talking to some manifestation of Venus! The morning star, the evening star--Semsta was somehow *inside* this manifestation and conversing with them. "After so many years of stasis, there was a need for transformation. A radical transformation that requires some time alone. Some time in the darkness so that light can be seen again. A time away from the world for metamorphosis. Transformation...transition...I needed to come here to do that. Once this time is over I will return as the morning star. But going forward, there will be this interim period where I leave the above and return here. Because transformation and transition is never complete."

> Semsta was stunned. Here was Venus saying exactly what they had been feeling for the past few weeks. "Dearest Venus, this is certainly what drew me here. I have been feeling that we are stuck, that we are repeating the same things without change. We need something new. I've been yearning for some kind of transformation, but too many in my cohort and others are content in simply continuing what we've always done. Yet I fear that if we don't transform, then we are doomed to remain as we are, forever."

> "Then you are exactly the right person to have found me here," Venus replied. "You will be my messenger of this new direction. I know of your festivals around my movement from night to day. You will let people know that while I am gone is to be the time of transformation. Certainly transformations cannot be completed during my brief stays here, but when I am gone will be a propitious moment to either start or to end a transformation period. For of course there will be many of these throughout your lives. Do you understand?"

> "Yes Venus, I do. Thank you."

> "I will now transfer the equations to you so that your astrologers can predict my times away. Then, your festival periods will align, from this moment forward, with my travels in the night, in here, in the morning, and back again. While the cycles will continue endlessly, my periods here ensure that they always repeat, with a difference."

> Semsta felt a bit of a prick in their brain, certain of the transfer for the equations but unable to grasp them yet.

> "Now please, go, leave me be. The equations will let your astrologers predict me next return. I need my time alone, for the transformation, so that I can be amongst you and the rest of the entities in the universe again."

> Semsta blinked their eyes once...and was back on the surface, at the place where they initially tumbled into the void. Pinching themselves, Semsta winced and realized that indeed, it was not a dream.

May your openness towards me have not been just a dream,

K–

—

Dear A–,

I really don't know why I continue these games with you. Maybe it's the chase. Maybe I like being disappointed regularly. Maybe I am hopeless. Maybe I see something of myself in you. I truly think that if you would come to the dig, if you would see what am seeing every day...well, in full disclosure I've done an enactment to hopefully guide you here. As we all know enactments can't ever *force* you to do something, but rather nudge you in the direction that you were already planning on going, but perhaps didn't realize yet. So I'm hoping this enactment will work.

I think I might finish the story in this quanta...and then I don't know what I will write you about! Oh of course...the wonderful artifacts found in this dig.

> Semsta's travel back needs not be narrated. When they returned to the Henge, the astrologers were still there, in the same place that Semsta left them, still puzzling over their calcs. When Semsta came up to them, words began to come out of Semsta's mouth that they never had said before: "ecliptic", "derivative", "third-order", "ephemeris", along with sequences of numbers. The astrologers faces sparkled with excitement, for they understood the stream of phonemes from Semsta's mouth, even if Semsta didn't. Immediately after this possessed speech was done, the astrologers turned away and tapped furiously at their calcs. Semesta, suddenly very tired, went to the camping area at the Henge, set up camp, and slept peacefully for the first time in weeks.

> When they woke the next morning, they learned from the facilitating astrologer that a date for Venus' next rise in the morning light had been calculated. Semesta narrated their experiences with Venus in the...underworld? Void? Transplace? Transplace. The facilitating astrologer seemed a bit taken aback, surprised that this person, Semsta, who was not trained at all in cosmic things, had had an encounter with one of the most important entities of the cosmos. However, the facilitating astrologer, being wise, knew an omen when they saw it, and accepted that Semesta was here to narrate a new way of living in the world.

> Immediately after this conversation the astrologers sent macros across the land, calling the cohorts back to the Henge with the new date of the rising of Venus in the morning. Semesta stayed at the Henge, and didn't learn until later about the confusion that this new macro caused. People were missing most of the story--namely Semsta's encounter with Venus--and thus they were worried. So it was no surprise that when all of the cohorts arrived just before the appointed date the mood was more tense than usual for an Apex Festival.

> In conversation with the astrologers it was decided that Semsta would address the crowd in the morning darkness, just before the purported rising of Venus. Semsta walked through the crowd in the evening light the night before Venus' rise again, as an attempt to calm some of the worry. They didn't want to give too much away though, so as to not start rumors that would spread like wildfire through the cohorts.

> Time passed quickly to just a segment before the expected rise of Venus again. Everyone--even the youngest ones--from the cohorts was oriented east. Semsta was with the astrologers at the front of the crowd. As their voice was a bit soft due to the circumstances of their first passage they spoke through a speaking trumpet.

> "I never expected to be standing in front of you like this," they began. "Yet recently I've been feeling as if something is not right. That we were living too much in a time of stasis. That we needed some change. I came here with all of you a few weeks ago for the Apex Festival. But I stayed afterwards, much to the dismay of my cohort, because I sensed a need to figure something out." Semsta searched for Jesp's face in the crowd, but couldn't find it in the darkness. "I began walking towards the east, unsure of what I would find. I was looking for Venus. And I found them."

> Murmurs of incomprehension filled the crowd.

> "I fell into a space that I now call the Transplace. And I call it this because of what Venus told me. That even Venus needs things to change. That Venus needs times for transformation. And that we all should build that into our lives. So, Venus, after each period in the morning or evening light, will take some time away in the Transplace to begin or end some kind of transformation. What that transformation is is perhaps not known until the moment it begins, or perhaps it is something that is planned for for a long time. All that is important is that when Venus undergoes their transformation, we will do so as well. We can't continue living and doing the same thing for years after years after years. We need to choose and direct our transformations. We don't know what our bodies can do until we try to transform them into something else. Let's see what we can do and how much of this amazing cosmos we can experience."

> During Semsta's speech there were definitely cries of dissent (from the more conservative people in the cohorts), but as Semsta was nearing the end the earlier incomprehension begin to shift into a mood of assent. And in a moment that would be retold for generations, as the sounds from Semsta's last sentence faded out, at the eastern horizon, just as had been predicted, and at just the time that had been foretold, Venus began to rise as the morning star.

> Ever since, the name Semsta was chosen by those who were seemingly called to facilitate transformation and transition.

Wait. The entangled quanta has been broken! You've opened it. Does this mean you're coming to visit?

In anticipation,

K–

—

Dear A–,

I know it's going to be an arduous journey for you. I know the difficulties it presents to you. I know that it might not live up to your expectations. But I am beyond excited for your arrival. I've told everyone that you're coming to visit the dig, and maybe to even work on it with us. People are so excited to be able to meet you, as you have quite the reputation, given your accomplishments. 

But before you leave, maybe it's best if I give you an outline of some of the things we've found, so you can decide if you need to bring any tools or study materials:

* There's a module that seems to combine different signals together using ancient Boolean logic
* There's something that seems to respond to vibration, but exactly how, we don't know yet
* There are some strange markings on a few of the modules that reference what appear to be old designs for reflectarrays
* And of course the one I told you about, a few quantas ago, called XENOKINETICS, which seems to involve a close attention to the ways cycles overlap and intersect.

I'd definitely bring any information you have on trans-temporal history, as we do believe the makers of these artifacts were trying to twist the timestreams, even if they didn't quite accomplish it like we can. Also, I know you might have resources on some of these ancient technologies in your library, so bring anything you might have on:

* diode drops
* piezoelectricity
* electrical flow through the skin in the absence of implanted biofeons
* ancient forms of tuning frequencies
* silver chloride electrodes
* pre-Transitioning Times antennas

Should I arrange for a tent to be made up for you? You're more than welcome to stay in mine…

Soon,

K–

—

Dear A–,

Okay, I'll make sure to collect the leaves for your favorite tea and have it ready for your arrival. I brought the kettle that you like, in some hope that you might actually come and visit, even though I knew it was so highly unlikely. Can you bring those berries that I adore?

See you soon love,

K–

