---
title: "SELESTIUM MODULAR BLANK PANEL"
description: "the triple aspect of venus"
slug: ""
featured: ""
keywords: ""
tagline: ""
categories: 
    - "Index"
    - "modules"
date: 2023-08-15T14:04:06+02:00
draft: false
thumbnail: 
    url: "/images/selestium_modular_blank_panel.jpg"
layout: minimal
---

{{< mgallery list="images/*" cols=3 type="grid" >}}

*ARCHIVAL DISCLAIMER*: As is common with the other artifacts from the dig AG-105, we believe this "module" also comes from "selestium modular", the producer of a set of sound production modules from the early 21st century. For how to link the modules together, please refer to the appropriate chapters of the *selestium* or the æther-net. Our re-creation of it for this edition has tried to follow as closely as possible the design of the module found in the dig, but built using contemporary processes. As a result, there may be some minor deviations from the versions that are seen in the archives.

# Rationale

This "module" consists only of a panel with no associated electronic circuitry, not even of the un-powered sort. For a long time we have been flummoxed as to what the purpose of such a module could have been. We have not been able to make conclusive determinations, but we have come up with the following possibilities:

* Such a panel, given the designs on the front of it, could have been used to incorporate protective or affirmative "sigils" into the sonic system;
* Such a panel could have been included as part of a ritual space in someone's abode;
* Such a panel could have been displayed as a form of artwork elsewhere in the abode.

The imagery on the front of this panel bears a nearly identical resemblance to the *selestium modular* logo that we see all over the artifacts in dig AG-105. Thus we think this panel was a representation of the same iconography related to Venus: Venus as an evening star, Venus as a morning star, and Venus in the transplace. For more information about these aspects, please read the story about the history of dig AG-105 in the selestium, also available here: <https://modular.selestium.net/pages/dig_ag-105/>.


# Datums

These are the measurements we have for the SELSTIUM MODULAR blank panel, in the units of the time:

* Width: 18HP (91,1mm)
* Power: unpowered

# Purchasing

You can <a href="https://vending.selestium.net/product/selestium-modular-blank-panel/" target="_blank">purchase SELESTIUM MODULAR blank panel</a> from the <a href="https://vending.selestium.net/" target="_blank">selestium vending</a> website.
