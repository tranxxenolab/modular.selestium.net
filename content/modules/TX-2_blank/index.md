---
title: "TX-2 BLANK PANEL"
description: "design from an ancient proposed cubesat mission"
slug: ""
featured: ""
keywords: ""
tagline: ""
categories: 
    - "Index"
    - "modules"
date: 2023-08-15T14:03:54+02:00
draft: false
thumbnail: 
    url: "/images/TX-2_blank_panel.jpg"
layout: minimal
---


<!-- {{< mimage src="images/TX-2_blank_panel.jpg" caption="Blank panel re-created from the design of an ancient proposed cubesat mission" >}} -->

{{< mgallery list="images/*" cols=3 type="grid" >}}

*ARCHIVAL DISCLAIMER*: As is common with the other artifacts from the dig AG-105, we believe this "module" also comes from "selestium modular", the producer of a set of sound production modules from the early 21st century. For how to link the modules together, please refer to the appropriate chapters of the *selestium* or the æther-net. Our re-creation of it for this edition has tried to follow as closely as possible the design of the module found in the dig, but built using contemporary processes. As a result, there may be some minor deviations from the versions that are seen in the archives.

# Rationale

This "module" consists only of a panel with no associated electronic circuitry, not even of the un-powered sort. For a long time we have been flummoxed as to what the purpose of such a module could have been. We have not been able to make conclusive determinations, but we have come up with the following possibilities:

* Such a panel, given the designs on the front of it, could have been used to incorporate protective or affirmative "sigils" into the sonic system;
* Such a panel could have been included as part of a ritual space in someone's abode;
* Such a panel could have been displayed as a form of artwork elsewhere in the abode.


For a long time we were confused as to what the markings on the front meant. While they had a certain resemblance to our *reflectarray* antennas, which allow for complicated antenna structures to be implemented on a flat panel, this was a design that we had never seen before. We weren't even sure it was a functioning antenna.

Luckily A-- is excellent at archival research and was able to find brief references to a proposed interplanetary mission just before the Transitioning Times called *TX-2: MOONSHADOW*. We've been able to re-create some aspects of the original æther-net site that hosted information about this project: <https://tranxxenolab.net/projects/tx-2/>. From what we understand this was an artistic project that actually proposed a non-human space mission to Mars...an early attempt to suggest that perhaps extraterrestrial space travel might be more suited to organisms that can handle extreme environments, rather than humans (although we are of course working to remedy this, even though we have much more work to do to ensure that transformations will allow for extraterrestrial space living without harming the entities in offworld environments). The designs on this panel, then, reflect some of the designs of "antennas" for *TX-2: MOONSAHDOW*, and while we don't think the reflecarrays here were made to be a functional antenna, we do believe it was designed to be some kind of protective sigil for the *MOONSHADOW* mission. As such, we believe its use as a panel in the *selestium modular* lineup was for the same purpose.

# Datums

These are the measurements we have for the TX-2 blank panel, in the units of the time:

* Width: 12HP (60,6mm)
* Power: unpowered

# Purchasing

You can <a href="https://vending.selestium.net/product/tx-2-blank-panel/" target="_blank">purchase TX-2 blank panel</a> from the <a href="https://vending.selestium.net/" target="_blank">selestium vending</a> website.
