---
title: "XENOKINETICS"
description: "exploring temporal dynamics from the quantum to the cosmic"
slug: ""
image: ""
keywords: ""
tagline: ""
categories: 
    - "Index"
    - "modules"
date: 2023-07-27T11:46:25+02:00
thumbnail: 
    url: "/images/000_XENOKINETICS.jpg"
layout: minimal
draft: false
---

{{< mgallery list="images/*" >}}

XENOKINETICS is in the final stages of development, based on the information we found in dig AG-105. More information will be posted here shortly. If you would like to be informed when XENOKINETICS is available, please sign up for the "back in stock" notification on the [selestium vending website](https://vending.selestium.net/product/xenokinetics/).
