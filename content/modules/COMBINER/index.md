---
title: "COMBINER"
description: "mixing of signals, audio-rate or control"
slug: ""
featured: ""
keywords: ""
tagline: ""
categories: 
    - "Index"
    - "modules"
date: 2023-07-27T11:45:03+02:00
draft: false
thumbnail: 
    url: "/images/000_COMBINER.jpg"
layout: minimal
---

{{< mgallery list="images/*" >}}

*ARCHIVAL DISCLAIMER*: As is common with the other artifacts from the dig AG-105, we believe this "module" also comes from "selestium modular", the producer of a set of sound production modules from the early 21st century. For how to link the modules together, please refer to the appropriate chapters of the *selestium* or the æther-net. Our re-creation of it for this edition has tried to follow as closely as possible the design of the module found in the dig, but built using contemporary processes. As a result, there may be some minor deviations from the versions that are seen in the archives.

# Rationale

Through extensive studies of various modules found in dig AG-105, we have realized that one of the most basic practices was the combination of signals together, what was called at the time "mixing". Signals that produced audible outputs were mixed; signals that controlled other signals ("control voltages") were mixed; in short, the mixing and combining of signals together was a constant need. Because of this, it's no surprise that we would also find in this dig many examples of the aptly-named "COMBINER" module.

*COMBINER* is quite basic: there are three knobs at the top, three jacks for inputs, and one jack for an output. Input signals flow through the jacks, and the corresponding knobs would control the levels of the input signals. Based on these levels, the output would be a weighted sum of the inputs. Simple as that.

Such a module, however, required power to properly sum the signals together. While the summing could occur without power, power was needed to keep the sums at appropriate levels for further transport within the complete system.

However, there is an addition note to make: for this edition we have used "high-precision" resistors in the feedback elements of the mixing circuit. This should allow for more accurate mixing, especially when it involves pitch information.


# Datums

These are the measurements we have for the COMBINER module, in the units of the time:

* Width: 3HP (14,9mm)
* Depth: 25mm
* Current:
    * +12V: 4mA
    * -12V: 4mA

As is customary from the time period in question, align the red stripe on your power cable with the white line on the circuit board.

# Quick usage

*ARCHIVAL NOTE*: As with all of the modules from "selestium modular" very little written text is found on the front of the modules; instead, custom iconography is present, the basics of which are found at the beginnings of each chapter in the *selestium* for the modules. 

While the COMBINER module can combine both audible signals as well as control voltage signals together, it often does not make sense to combine an audible signal *with* a control voltage signal. It's best to combine like signals with like, although nothing prevents you from mixing them up in specialised circumstances.

* Combine outputs from three audio-rate oscillators together.
* Combine three different low-frequency oscillators together to create a hyper-complex LFO.
* Use a constant control voltage at one of the inputs to add an offset to two other oscillating signals.
* Use a sequence of control voltages (such as from a sequencer) and add a constant voltage to move the sequence up and down a specific number of intervals, such as an octave.
* Do simple arithmetic with three operands.

# Manual

You can download the manual <a href="COMBINER_manual.pdf">here</a>.

# Purchase

You can <a href="https://vending.selestium.net/product/combiner/" target="_blank">purchase COMBINER</a> from the <a href="https://vending.selestium.net/" target="_blank">selestium vending</a> website.
