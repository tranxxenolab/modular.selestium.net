---
title: "MULTIPLIER+"
description: "buffered multiple with normalisation"
slug: ""
image: ""
keywords: ""
tagline: ""
categories: 
    - "Index"
    - "modules"
date: 2023-07-27T11:46:18+02:00
thumbnail: 
    url: "/images/000_MULTIPLIERplus.jpg"
layout: minimal
draft: false
---

{{< mgallery list="images/*" >}}

*ARCHIVAL DISCLAIMER*: As is common with the other artifacts from the dig AG-105, we believe this "module" also comes from "selestium modular", the producer of a set of sound production modules from the early 21st century. For how to link the modules together, please refer to the appropriate chapters of the *selestium* or the æther-net. Our re-creation of it for this edition has tried to follow as closely as possible the design of the module found in the dig, but built using contemporary processes. As a result, there may be some minor deviations from the versions that are seen in the archives.

# Rationale

Signals flowing through a system do not flow without resistance. No matter how "perfect" the cable is, there will be some voltage drop along it. As well, some modules, such as JOINER8x and JOINER3xby2, cause a voltage drop from input to output due to the way they were designed. As a result, it's sometimes useful to create *exact* (as exact as possible, that is) copies of a signal without any further voltage drop, as if the voltage gets too low other modules in the signal chain might not function properly.

Enter this module, MULTIPLIER+, which acts as what was called a "buffered multiple". This means that an input signal is "buffered", using external power, so that a copy can be made without any voltage loss. As well, the module ensures that the *impedance* of the signal is at the right level for further processing. MULTIPLIER+ requires external power to do this; there are other kinds of multiples, *passive* ones, that don't use power but also don't provide the buffering and impedance transformation that MULTIPLIER+ does.

The module has two separate sections, with normalisation between them. The top section has one input signal and three copies at three different outputs. If there is no signal present at the input of the bottom section, then the output of the top section is normalised to the input of the bottom, and there are thus 2 more copies produced, allowing for a total of 6 outputs from one input. If there *is* a signal input at the top of the bottom section, then MULTIPLIER+ becomes a dual 1 input, 3 output buffered mult.

# Datums

These are the measurements we have for the MULTIPLIER+ module, in the units of the time:

* Width: 3HP (14,9mm)
* Depth: 25mm
* Current:
    * +12V: 12mA
    * -12V: 12mA


# Quick usage

**ARCHIVAL NOTE**: As with all of the modules from "selestium modular" very little written text is found on the front of the modules; instead, custom iconography is present, the basics of which are found at the beginnings of each chapter in the *selestium* for the modules. 

There is not much creative to be done with MULTIPLIER+, at least from what we've been able to discover in our research. It is a utility module in the barest sense of that word.

# Purchasing

You can <a href="https://vending.selestium.net/product/multiplier/" target="_blank">purchase MULTIPLIER+</a> from the <a href="https://vending.selestium.net/" target="_blank">selestium vending</a> website.
