---
slug: a-propos
title: À propos
description: Un thème de blog et de documentation pour Hugo basé sur Bootstrap 5.
date: 2023-02-17
updated: 2023-08-02
showComments: false
---

<p class="text-center"><svg class="img-fluid w-50"><use href="/img/logo_var.svg#logo"></use></svg></p>

<section class="section section-sm mt-5">
  <div class="container-fluid">
    <div class="row justify-content-center text-center">
    <div class="row justify-content-center text-center">
      <div class="col-lg-4">
        {{< icon fab bootstrap fa-2xl >}}
        <h2 class="h4">Framework Bootstrap</h2>
        <p>Créez des sites rapides et réactifs avec Bootstrap 5. Personnalisez facilement votre site avec les fichiers source Sass.</p>
      </div>
      <div class="col-lg-4">
        {{< icon fas magnifying-glass fa-2xl >}}
        <h2 class="h4">Recherche en texte intégral</h2>
        <p>Recherchez votre site avec FlexSearch, une bibliothèque de recherche en texte intégral avec zéro dépendances.</p>
      </div>
      <div class="col-lg-4">
        {{< icon fas code fa-2xl >}}
        <h2 class="h4">Outils de développement</h2>
        <p>Utilisez Node Package Manager pour automatiser le processus de construction et suivre les dépendances.</p>
      </div>
    </div>
  </div>
</section>

Les fonctionnalités supplémentaires incluent:

- Passer du mode clair au mode sombre
- Prise en charge de plusieurs langues
- Composants bootstrap réutilisables à travers des codes et partiels configurables
- Documentation versionnée, incluant une navigation latérale et un sélecteur de version.
- Commentaires intégrés via une intégration légère avec GitHub via [utteranc.es]({{< param "links.utterances" >}})
- Gestion d'images adaptatives pour plusieurs tailles d'écran et résolutions.
- Résultats de recherche optimisés, obtenant un score de 100 points pour le référencement (SEO) sur [PageSpeed Insights]({{< param "links.pagespeed" >}}).
- Sécurisé par défaut, obtenant un score A+ au test [Mozilla Observatory]({{< param "links.observatory" >}})
  {.tickmark}

Hinode est inspiré par les thèmes suivants:

- [Blist](https://github.com/apvarun/blist-hugo-theme) - Un thème de blog pour Hugo basé sur Tailwind CSS.
- [Doks](https://github.com/h-enk/doks) - Un thème Hugo pour la création de sites de documentation sécurisés, rapides et optimisés pour le référencement (SEO), que vous pouvez facilement mettre à jour et personnaliser.
