---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
description: ""
slug: ""
featured: ""
keywords: ""
categories: 
    - "{{ replace .TranslationBaseName "-" " " | title }}"
    - "page"
date: {{ .Date }}
draft: false
thumbnail: 
    url: "img/selestium002_square_purple.png"
layout: minimal
---
