---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
description: ""
slug: ""
featured: ""
keywords: ""
tagline: ""
categories: 
    - "{{ replace .TranslationBaseName "-" " " | title }}"
    - "modules"
date: {{ .Date }}
draft: false
thumbnail: 
    url: "img/selestium002_square_purple.png"
layout: minimal
---
