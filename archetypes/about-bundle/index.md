---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
description: ""
slug: ""
featured: ""
keywords: ""
categories: 
    - "{{ replace .TranslationBaseName "-" " " | title }}"
    - "about"
date: {{ .Date }}
draft: false
---
